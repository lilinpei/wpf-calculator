﻿using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using Expression = org.mariuszgromada.math.mxparser.Expression;

namespace wpf_calc_beta
{
    /// <summary>
    /// Interaction logic for CalcBeta.xaml
    /// </summary>
    public partial class CalcBeta
    {
        public CalcBeta() // Инициализация калькулятора
        {
            InitializeComponent();
            MathTb.Text = "0";
            ResultTb.Text = "0";
        }
        private void AddString(string s) => MathTb.Text += s; //Функция записи символа в пример
        private void RemoveString(int amt) => MathTb.Text = MathTb.Text.Remove(MathTb.Text.Length - amt);
        private int GetBr()
        {
            var oBr = 0; //Кол-во открывающихся скобок
            var cBr = 0; //Кол-во закрывающихся скобок
            foreach (var c in MathTb.Text) //Узнаем циклом сколько каких скобок
            {
                switch (c)
                {
                    case '(':
                        oBr++;
                        break;
                    case ')':
                        cBr++;
                        break;
                }
            }
            return oBr - cBr;
        }
        private string BrCount(string s)
        {
            for(var i = 0; i < GetBr(); i++) //Добавляем в пример отсутствующие скобки циклом
            {
                s += ')';
            }
            return s;
        }
        private static bool CheckForAction(string s) => s.EndsWith("+") || s.EndsWith("-") || s.EndsWith("×") || s.EndsWith("÷") || s.EndsWith("√("); //Проверка примера на последний знак действия
        private void EvalExpression(string expr) //Вычисление
        {
            while(CheckForAction(expr))
            {
                if (CheckForAction(expr)) expr = expr.Remove(expr.Length - 1); //Если последний символ в примере действие, то убираем его
                if (expr.EndsWith("√") || expr.EndsWith("(") || expr.EndsWith(")")) expr = expr.Remove(expr.Length - 1); //Доп. проверка
            }
            expr = BrCount(expr);
            var res = new Expression(expr.Replace("×", "*").Replace("÷", "/").Replace("√", "sqrt").Replace("π", "pi")).calculate();
            ResultTb.Text = !double.IsNaN(res) ? res.ToString(CultureInfo.CurrentCulture).Replace(",", ".") : "0";
        }
        private void NumClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button) e.Source).Content.ToString();
            if (MathTb.Text == "0") RemoveString(1);
            if (MathTb.Text.EndsWith(")")) AddString("×"); //Если нажали число перед скобкой ставится умножение
            AddString(s);
            EvalExpression(MathTb.Text);
        }
        private void ActClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button) e.Source).Content.ToString();
            if ((MathTb.Text.EndsWith("(") || MathTb.Text.EndsWith("^")) && s != "-") return; //Тут короче код отвечающий за минус после открывающейся скобки
            else if ((MathTb.Text.EndsWith("(-") || MathTb.Text.EndsWith("^-")) && s == "+") //Если нажали плюс, то минус после скобки убирается
            {
                RemoveString(1);
                return;
            }
            else if (MathTb.Text.EndsWith("(-") || MathTb.Text.EndsWith("^-")) return; //на случай чтобы не получить вместо минуса любое другое действие
            if (CheckForAction(MathTb.Text) || MathTb.Text.EndsWith(".")) RemoveString(1);
            AddString(s);
        }
        private void DotClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button) e.Source).Content.ToString();
            if (CheckForAction(MathTb.Text) || MathTb.Text.EndsWith("!") || MathTb.Text.EndsWith("π")) return;
            for (var i = MathTb.Text.Length-1; i >= 0; i--) //Цикл нужен для того чтобы всякие гении не ставили в одном числе две точки
            {
                if (CheckForAction(MathTb.Text[i].ToString())) break;
                if (MathTb.Text[i].ToString() == s) return;
            }
            AddString(s);
        }
        private void EqualClick(object sender, RoutedEventArgs e)
        {
            MathTb.Text = ResultTb.Text; //Пока нажатие на равно заменяет пример на результат от решения
        }
        private void BracketClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button) e.Source).Content.ToString();
            if (GetBr() <= 0 && s != "(") return;
            if (!CheckForAction(MathTb.Text) && MathTb.Text != "0" && s != ")" && !MathTb.Text.EndsWith("(")) AddString("×"); //Если после числа нет действий то ставим умножение
            if (CheckForAction(MathTb.Text) && MathTb.Text.EndsWith(")") || MathTb.Text.EndsWith("(") && s == ")") return; //Проверка чтобы не ставили закрывающую скобку после действия и открывающей скобки
            if (MathTb.Text.EndsWith(".") || MathTb.Text == "0") RemoveString(1); //Если перед скобкой точка, или весь пример это 0, то убираем точку/ноль
            AddString(s);
        }
        private void PowClick(object sender, RoutedEventArgs e)
        {
            const string s = "^";
            if (MathTb.Text.EndsWith("√(") || MathTb.Text.EndsWith("(") || MathTb.Text.EndsWith("^")) return;
            if (CheckForAction(MathTb.Text) || MathTb.Text.EndsWith(".")) RemoveString(1);
            AddString(s);
        }

        private void SqrtClick(object sender, RoutedEventArgs e)
        {
            const string s = "√(";
            if (MathTb.Text.EndsWith(".") || MathTb.Text == "0") RemoveString(1);
            if (CheckForAction(MathTb.Text) || MathTb.Text == string.Empty || MathTb.Text.EndsWith("("))
            {
            } // Ничего не делаем в этих случаях
            else AddString("×"); //Тут проверка чтобы знак умножения ставился сам в определенных случаях

            AddString(s);
        }

        private void ConstShow(object sender, RoutedEventArgs e)
        {
            ConstGrid.Visibility = Visibility.Visible;
            NumGrid.Visibility = Visibility.Hidden;
        }

        private void ConstBack(object sender, RoutedEventArgs e)
        {
            ConstGrid.Visibility = Visibility.Hidden;
            NumGrid.Visibility = Visibility.Visible;
        }
        private void ConstClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button)e.Source).Content.ToString();
            if (MathTb.Text == "0" || MathTb.Text.EndsWith(".")) RemoveString(1);
            if (!CheckForAction(MathTb.Text) && MathTb.Text.Length != 0 && !MathTb.Text.EndsWith("^") && !MathTb.Text.EndsWith("(")) AddString("×");
            AddString(s);
            EvalExpression(MathTb.Text);
        }
        private void RemoveClick(object sender, RoutedEventArgs e)
        {
            if (MathTb.Text.Length == 1) MathTb.Text = "0";
            else if (MathTb.Text.EndsWith("√(") || MathTb.Text.Length == 2 && MathTb.Text[0] == '-') RemoveString(2);
            else if (MathTb.Text.EndsWith("sin(") || MathTb.Text.EndsWith("cos(") || MathTb.Text.EndsWith("tan(") || MathTb.Text.EndsWith("ctg(")) RemoveString(4);
            else RemoveString(1);
            if (MathTb.Text == string.Empty)
            {
                MathTb.Text = "0";
            }
            EvalExpression(MathTb.Text);
        }
        private void ClearClick(object sender, RoutedEventArgs e)
        {
            MathTb.Text = "0";
            ResultTb.Text = "0";
        }
        private void FactClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button) e.Source).Content.ToString();
            if (MathTb.Text.EndsWith(".") || MathTb.Text.EndsWith("!") || MathTb.Text.EndsWith("(") ||
                MathTb.Text.EndsWith(")") || CheckForAction(MathTb.Text)) return;
            AddString(s);
            EvalExpression(MathTb.Text);
        }
        private void FuncShow(object sender, RoutedEventArgs e)
        {
            FuncGrid.Visibility = Visibility.Visible;
            NumGrid.Visibility = Visibility.Hidden;
        }
        private void FuncBack(object sender, RoutedEventArgs e)
        {
            FuncGrid.Visibility = Visibility.Hidden;
            NumGrid.Visibility = Visibility.Visible;
        }

        private void FuncClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button)e.Source).Content.ToString();
            if (MathTb.Text == "0" || MathTb.Text.EndsWith(".")) RemoveString(1);
            if (!CheckForAction(MathTb.Text) && MathTb.Text.Length != 0 && !MathTb.Text.EndsWith("^") && !MathTb.Text.EndsWith("(")) AddString("×");
            AddString(s + "(");
            EvalExpression(MathTb.Text);
        }
    }
}
